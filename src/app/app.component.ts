import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { from } from 'rxjs';
import {
  SimilarService,
  UrlValue,
  ArrayValue,
  SubjectValue,
  SecValue,
} from './similar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title: SubjectValue[] = [];
  myData: SecValue[] = [];
  constructor(private http: HttpClient, private _service: SimilarService) {}
  ngOnInit() {
    this._service.myData.subscribe((x) => (this.title = x));
    this._service.mySecData.subscribe((x) => (this.myData = x));
  }
  sendValue() {
    this._service.sendData([{ ID: 3, Name: 'Sujith', character: 'bad' }]);
  }
  showMyValue() {
    this._service.sendMyData([
      {
        Name: 'Ajith',
        Bro: 'Tamil',
        Age: 23,
        Family: 'None',
        Ability: 'Communication skill',
      },
    ]);
  }
}

export class UserData {
  userId: number;
  id: number;
  title: string;
  body: string;
}
