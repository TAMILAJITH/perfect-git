import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { SimilarService } from './similar.service';
import { AppRoutingModule, routingComponent } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent, routingComponent],
  imports: [BrowserModule, HttpClientModule, AppRoutingModule],
  providers: [SimilarService],
  bootstrap: [AppComponent],
})
export class AppModule {}
