import { Component, OnInit } from '@angular/core';
import { SecValue, SimilarService } from '../similar.service';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css'],
})
export class DepartmentComponent implements OnInit {
  constructor(private _service: SimilarService) {}
  myData: SecValue[] = [];
  ngOnInit() {
    this._service.mySecData.subscribe((x) => (this.myData = x));
  }
}
