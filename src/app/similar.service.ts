import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SimilarService {
  constructor(private _http: HttpClient) {}

  public myData: Subject<SubjectValue[]> = new Subject<SubjectValue[]>();
  public mySecData: Subject<SecValue[]> = new Subject<SecValue[]>();

  sendData(data: SubjectValue[]) {
    this.myData.next(data);
  }
  sendMyData(data: SecValue[]) {
    this.mySecData.next(data);
  }
  private _httpValue: string = 'https://erp.arco.sa:65//api/TickettypeList';

  checkingValue(): Observable<UrlValue> {
    return this._http.get<UrlValue>(this._httpValue);
  }
}

export class CheckInterface {
  name: string;
  father: string;
}
export class UrlValue {
  Data: ArrayValue[];
}
export class ArrayValue {
  ID: number;
  Name: string;
}
export class SubjectValue {
  ID: number;
  Name: string;
  character: string;
}
export class SecValue {
  Name: string;
  Bro: string;
  Family: string;
  Age: number;
  Ability: string;
}
